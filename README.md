# Create Manjaro images

## Create root filesystem
```
sudo bash create-rootfs.sh
```

## Build Docker image

### manjaro-base

```
docker build -t manjaro-base -f Dockerfile.base .
```

### manjaro-devel

```
docker build -t manjaro-devel -f Dockerfile.devel .
```