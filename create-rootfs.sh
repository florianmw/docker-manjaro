#!/bin/bash

[ "$EUID" != "0" ] && echo "Please run as root" && exit 2

toolsInstalled=false
PACMAN_DEFAULT=/usr/share/manjaro-tools/pacman-default.conf

if ! pacman -Q manjaro-tools-base &>/dev/null
then
  pacman -Sy --noconfirm manjaro-tools-base
  toolsInstalled=true
fi

tmpdir=$(mktemp -d)
trap "rm -rf $tmpdir" EXIT

cat $PACMAN_DEFAULT rootfs/etc/pacman.d/noextract.conf >| rootfs/etc/pacman.conf
env -i basestrap -C rootfs/etc/pacman.conf -cdGM "$tmpdir" $(cat packages)
cp --recursive --preserve=timestamps --backup --suffix=.pacnew rootfs/* ${tmpdir}/
tail -n+5 exclude > ${tmpdir}/exclude
ls ${tmpdir} | xargs tar --numeric-owner --xattrs --acls --exclude-from=exclude -C ${tmpdir} -czf rootfs.tar.gz

if $toolsInstalled
then
  pacman -Rsc --noconfirm manjaro-tools-base
fi
